package sbu.cs;
import java.util.Random;
public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        String password = "";
        //2020 for random seed to have a better Pseudorandom generataing
        Random rand = new Random(2020);
        for(int i = 0; i < length; i++) {
            password += (char)('a' + rand.nextInt(26));
        }
        return password;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if(length < 3)
            throw new Exception();
        //2020 for random seed to have a better Pseudorandom generataing
        Random rand = new Random(2020);
        //reserve a(for Emphasis on singularity) place for a Special characters
        int specialCharacterPlace = rand.nextInt(length);
        //reserve a(for Emphasis on singularity) place for a number
        int numberPlace = rand.nextInt(length);
        while(specialCharacterPlace == numberPlace) {
            numberPlace = rand.nextInt(length);
        }
        //All characters varies 32-126 on ascii table
        String specialCharacters = " !\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~";
        //Making a strong password
        String password = "";
        for(int i = 0; i < length; i++) {
            if(i == numberPlace)
                password += (char)('0' + rand.nextInt(10));
            else if(i == specialCharacterPlace)
                password += specialCharacters.charAt(rand.nextInt(specialCharacters.length()));
            else {
                password += (char)(32 + rand.nextInt(126 - 32 + 1));
            }
        }
        return password;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        ExerciseLecture4 ExerciseLecture4Object = new ExerciseLecture4();
        int i = 1;
        long sum = 0;
        long bin;
        while(sum <= n) {
            long fiboI = ExerciseLecture4Object.fibonacci(i);
            String IFiboBinary = Long.toBinaryString(fiboI);
            bin = 0;
            for(int j = 0; j < IFiboBinary.length(); j++) {
                if(IFiboBinary.charAt(j) == '1')
                    bin++;
            }
            sum = fiboI + bin;
            if(sum == n)
                return true;
            i++;
        }
        return false;
    }
}
