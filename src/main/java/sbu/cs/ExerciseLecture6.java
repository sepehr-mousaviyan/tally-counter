package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for(int i = 0; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        for(int i = 0; i < arr.length / 2; i++) {
//            int temp = arr[i];
//            arr[i] = arr[arr.length - i - 1];
//            arr[arr.length - i - 1] = temp;
            arr[i] = arr[i] + arr[arr.length - i - 1];
            arr[arr.length - i - 1] = arr[i] - arr[arr.length - i - 1];
            arr[i] = arr[i] - arr[arr.length - i - 1];
        }
        return arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        final int numberOfRows1 = m1.length;
        final int numberOfRows2 = m2.length;
        final int numberOfCoulmn1 = m1[0].length;
        final int numberOfCoulmn2 = m2[0].length;
        if(numberOfCoulmn1 != numberOfRows2)
            throw new RuntimeException();
        double[][] product = new double[numberOfRows1][numberOfCoulmn2];
        for(int i = 0; i < numberOfRows1; i++) {
            for(int j = 0; j < numberOfCoulmn2; j++) {
                product[i][j] = 0;
                for(int k = 0; k < numberOfCoulmn1; k++) {
                    product[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return product;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> list = new ArrayList<>();
        for(int i = 0; i < names.length; i++) {
            List<String> row = new ArrayList<>();
            for(int j = 0; j < names[i].length; j++)
                row.add(names[i][j]);
            list.add(row);
        }
        return list;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> list = new ArrayList<>();
        for(int i = 2; i <= n; i++) {
            if(n % i == 0) {
                boolean isPrime = true;
                for(int j = 2; j < i; j++) {
                    if(i % j == 0) {
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime)
                    list.add(i);
                n /= i;
            }
        }
        return list;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> list = new ArrayList<>();
        StringBuilder word = new StringBuilder();
        int whichWord = 0;
        boolean isFirstCharacter = true;
        for(int i = 0; i < line.length(); i++) {
            boolean isLowerCase = 'a' <= line.charAt(i) && line.charAt(i) <= 'z';
            boolean isUpperCase = 'A' <= line.charAt(i) && line.charAt(i) <= 'Z';
            if(isLowerCase || isUpperCase) {
                if(isFirstCharacter) {
                    whichWord++;
                    word = new StringBuilder();
                    word.append(line.charAt(i));
                    list.add(word.toString());
                    isFirstCharacter = false;
                }
                else {
                    word.append(line.charAt(i));
                    list.set(whichWord - 1, word.toString());
                }
            }
            else
                isFirstCharacter = true;
        }
        return list;
    }
}
